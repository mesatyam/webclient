package com.webclient;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;

@RestController
public class webclient {
    WebClient web = WebClient.create("http://localhost:8080/");

    @GetMapping(value = "/done")
    public Flux<Boolean> docalll() {
        return web.get()
                .uri("/streamflow")
                .retrieve()
                .bodyToFlux(Boolean.class)
                ;
    }

}
